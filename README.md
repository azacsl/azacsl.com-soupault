# ACSL Website

Transfer of the Advanced Cyber Systems Lab website from [Zola](https://www.getzola.org/) to [Soupault](https://soupault.app/).

## Transfer Process

This process should work on any "conventional" site i.e. one that is not primarily JavaScript like React.

### Download

Download the existing site:

```zsh
wget -mEk https://www.azacsl.com
```

Options:

* `-m, --mirror`: Turn on options suitable for mirroring.  This option turns on recursion and time-stamping, sets infinite recursion depth and keeps FTP directory listings.  It is currently equivalent to `-r -N -l inf --no-remove-listing`.
* `-E, --adjust-extension`: If a file of type `application/xhtml+xml` or `text/html` is downloaded and the URL does not end with the regexp `\.[Hh][Tt][Mm][Ll]?`, this option will cause the suffix `.html` to be appended to the local filename. This is useful, for instance, when you're mirroring a remote site that uses `.asp` pages, but you want the mirrored pages to be viewable on your stock Apache server. Another good use for this is when you're downloading CGI-generated materials. A URL like `http://site.com/article.cgi?25` will be saved as `article.cgi?25.html`.
* `-k, --convert-links`: After the download is complete, convert the links in the document to make them suitable for local viewing.  This affects not only the visible hyperlinks, but any part of the document that links to external content, such as embedded images, links to style sheets, hyperlinks to non-HTML content, etc. ... Because of this, local browsing works reliably: if a linked file was downloaded, the link will refer to its local name; if it was not downloaded, the link will refer to its full Internet address rather than presenting a broken link. The fact that the former links are converted to relative links ensures that you can move the downloaded hierarchy to another directory.

### Cleanup

* Rename CSS files
* Remove Atom feed
* Remove sitemap

### Check Links

Run [lychee](https://github.com/lycheeverse/lychee#readme=): Fast, async, stream-based link checker written in Rust. Finds broken URLs and mail addresses inside Markdown, HTML, reStructuredText, websites and more!

```zsh
soupault && lychee --offline build
```

### Correct Links

Use [ruplacer](https://github.com/your-tools/ruplacer#readme=) to correct the broken css links.

```zsh
ruplacer --go 'abridge.css%3Fh=acd38643ab40dd4ee0aa4b890a0128da58e7e727121c1b410bdc061935438abf.css' 'abridge.css' site;
ruplacer --go 'acsl.css%3Fh=db69b4d34729920beb57837d20ad6fd197a3bd556fb2716b6d0633b0923cfc30.css' 'acsl.css' site;
ruplacer --go 'comet.css%3Fh=2c13da18e2bdf3b6414f62401ff532fd7a7c86d1116da672537134f3028d5de1.css' 'comet.css' site;
```

Note: It's likely that the specific hash strings will look different for you.

Use [ruplacer](https://github.com/your-tools/ruplacer#readme=) to correct the broken calendar links.

```zsh
ruplacer --go calendar.html calendar site
```

### Prettify Code

Working with properly formatted code is a lot easier. Determining structure and nesting is far easier and diffing code is more comprehensible.

We'll use [Prettier](https://prettier.io/) to clean up the HTML, CSS, and JavaScript.

```zsh
prettier -w site/**/*.{html,css,js}
```

## Convert to Haml

Use the following shell loop to convert the html pages to [Haml](https://haml.info/).

```zsh
for file in $(ls site/**/*.html)
do
	echoc -f info "converting: $file --> ${file:r}.haml"
	htmlq -f "$file" main | html2haml -s --no-erb --html-attributes > "${file:r}.haml"
	git rm "$file"
	git add "${file:r}.haml"
done
```
